package org.example.samplePackage.employeeException.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class employeeExceptionController {
    @ExceptionHandler(value = employeeNotFound.class)
    public ResponseEntity<Object> exception(employeeNotFound e) {
        return new ResponseEntity<>(e.getCustomMessage(), HttpStatus.NOT_FOUND);
    }
}
