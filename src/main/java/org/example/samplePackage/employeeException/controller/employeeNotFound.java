package org.example.samplePackage.employeeException.controller;

public class employeeNotFound extends RuntimeException{
    private String customMessage;

    public employeeNotFound(String customMessage) {
        this.setCustomMessage(customMessage);
    }

    public String getCustomMessage() {
        return customMessage;
    }

    public void setCustomMessage(String customMessage) {
        this.customMessage = customMessage;
    }
}
