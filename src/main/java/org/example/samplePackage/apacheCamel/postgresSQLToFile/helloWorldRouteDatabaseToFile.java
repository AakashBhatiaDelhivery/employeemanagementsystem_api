package org.example.samplePackage.apacheCamel.postgresSQLToFile;

import org.apache.camel.builder.RouteBuilder;

public class helloWorldRouteDatabaseToFile extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        System.out.println("Hello World! this message is generated from helloWorldRoute of apacheCamel.postgresSQLToFile package");
    }
}
