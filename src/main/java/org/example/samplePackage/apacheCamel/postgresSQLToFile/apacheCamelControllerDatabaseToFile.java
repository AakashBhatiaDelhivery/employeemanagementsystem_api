package org.example.samplePackage.apacheCamel.postgresSQLToFile;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@RestController
public class apacheCamelControllerDatabaseToFile {
    @Autowired
    private org.example.samplePackage.apacheCamel.postgresSQLToFile.copyAllEmployeesRouteDatabaseToFile copyAllEmployeesRouteDatabaseToFile;
    /*@Autowired
    private org.example.samplePackage.apacheCamel.fileToRabbitMQ.copySpecificFilesRouteFileToRabbitMQ copySpecificFilesRouteFileToRabbitMQ;
    @Autowired
    private org.example.samplePackage.apacheCamel.fileToRabbitMQ.copySpecificFilesWithContentRouteFileToRabbitMQ copySpecificFilesWithContentRouteFileToRabbitMQ;*/

    @RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/databaseToFile/hello")
    public String apacheCamelHelloDatabaseToFile(String[] args) {
        CamelContext helloWorldContext = new DefaultCamelContext();

        try {
            helloWorldContext.addRoutes(new helloWorldRouteDatabaseToFile());
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerDatabaseToFile's apacheCamelHelloDatabaseToFile method");
        }

        return "Printed Apache Camel's PostgresSQL message on console";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/databaseToFile/copyAllFiles/destination-{destination}")
    public String apacheCamelCopyAllFilesDatabaseToFile(@PathVariable String destination) throws IOException, TimeoutException {
        CamelContext copyAllFilesContext = new DefaultCamelContext();

        try {
            destination = destination.replace("&", "/");

            copyAllEmployeesRouteDatabaseToFile.setDestination(destination);
            copyAllFilesContext.addRoutes(copyAllEmployeesRouteDatabaseToFile);
            copyAllFilesContext.start();
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerFileToRabbitMQ's apacheCamelCopyAllFilesDatabaseToFile method");
        }

        return "All files from database are copied to destination folder = \"" + copyAllEmployeesRouteDatabaseToFile.getDestination() + "\"";
    }

    /*@RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/databaseToFile/copySpecificFiles/source-{source}-fileName-{fileName}")
    public String apacheCamelCopySpecificFilesToRabbitMQ(@PathVariable String source, @PathVariable String fileName) {
        CamelContext copySpecificFilesContext = new DefaultCamelContext();

        try {
            source = source.replace("&", "/");
            copySpecificFilesRouteFileToRabbitMQ.setSource(source);
            copySpecificFilesRouteFileToRabbitMQ.setFilter(fileName);
            copySpecificFilesRouteFileToRabbitMQ.send();
            copySpecificFilesContext.addRoutes(copySpecificFilesRouteFileToRabbitMQ);
            copySpecificFilesContext.start();
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerFileToRabbitMQ's apacheCamelCopySpecificFilesToRabbitMQ method");
        }

        return "All files with file name containing = \""+ copySpecificFilesRouteFileToRabbitMQ.getFilter() +"\" from source folder = \"" + copySpecificFilesRouteFileToRabbitMQ.getSource() + "\" are copied";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/databaseToFile/copySpecificFilesWithContent/source-{source}-content-{content}")
    public String apacheCamelCopySpecificFilesWithContentFileToRabbitMQ(@PathVariable String source, @PathVariable String content) {
        CamelContext copySpecificFilesWithContentContext = new DefaultCamelContext();

        try {
            source = source.replace("&", "/");
            copySpecificFilesWithContentRouteFileToRabbitMQ.setSource(source);
            copySpecificFilesWithContentRouteFileToRabbitMQ.setFilter(content);
            copySpecificFilesWithContentRouteFileToRabbitMQ.send();
            copySpecificFilesWithContentContext.addRoutes(copySpecificFilesWithContentRouteFileToRabbitMQ);
            copySpecificFilesWithContentContext.start();
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerFileToRabbitMQ's apacheCamelCopySpecificFilesWithContentFileToRabbitMQ method");
        }

        return "All files with content = \""+ copySpecificFilesWithContentRouteFileToRabbitMQ.getFilter() +"\" from source folder = \"" + copySpecificFilesWithContentRouteFileToRabbitMQ.getSource() + "\" are copied ";
    }*/
}
