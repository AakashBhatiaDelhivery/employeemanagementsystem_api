package org.example.samplePackage.apacheCamel.postgresSQLToFile;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.sql.SqlComponent;
import org.example.samplePackage.controller.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class copyAllEmployeesRouteDatabaseToFile extends RouteBuilder {
    private String destination;

    @Autowired
    private DataSource dataSource;

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public copyAllEmployeesRouteDatabaseToFile() {
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Bean
    public SqlComponent sql(DataSource dataSource) {
        SqlComponent sql = new SqlComponent();
        sql.setDataSource(dataSource);
        return sql;
    }

    @Override
    public void configure() throws Exception {
        from("direct:select").setBody(constant("select * from Employee")).to("jdbc:dataSource")
                .process(new Processor() {
                    public void process(Exchange xchg) throws Exception {
                        //the camel jdbc select query has been executed. We get the list of employees.
                        ArrayList<Map<String, String>> dataList = (ArrayList<Map<String, String>>) xchg.getIn().getBody();
                        List<Employee> employees = new ArrayList<Employee>();
                        System.out.println(dataList);
                        for (Map<String, String> data : dataList) {
                            Employee employee = new Employee();
                            employee.setEmployeeId(Integer.valueOf(data.get("employeeId")));
                            employee.setEmployeeName(data.get("employeeName"));
                            employee.setEmployeeDepartment(data.get("employeeDepartment"));
                            employees.add(employee);
                        }
                        xchg.getIn().setBody(employees);
                    }
                });
    }
}