package org.example.samplePackage.apacheCamel.fileToFile;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Service;

@Service
public class copyAllFilesRouteFileToFile extends RouteBuilder {
    private String source, destination;

    public copyAllFilesRouteFileToFile() {
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
    public void configure() throws Exception {
        from("file:" + source + "?noop=true")
                .to("file:" + destination);
    }
}
