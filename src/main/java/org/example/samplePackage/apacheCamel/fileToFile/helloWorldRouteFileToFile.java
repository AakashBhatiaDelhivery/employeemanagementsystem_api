package org.example.samplePackage.apacheCamel.fileToFile;

import org.apache.camel.builder.RouteBuilder;

public class helloWorldRouteFileToFile extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        System.out.println("Hello World! this message is generated from helloWorldRoute of apacheCamel.fileToFile package");
    }
}
