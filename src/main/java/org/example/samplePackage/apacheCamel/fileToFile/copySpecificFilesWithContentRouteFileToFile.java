package org.example.samplePackage.apacheCamel.fileToFile;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Service;

@Service
public class copySpecificFilesWithContentRouteFileToFile extends RouteBuilder {
    private String source, destination, filter;

    public copySpecificFilesWithContentRouteFileToFile() {
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public void configure() throws Exception {
        from("file:" + source + "?noop=true")
                .filter(body().convertToString().contains(filter))
                .to("file:" + destination);
    }
}
