package org.example.samplePackage.apacheCamel.fileToFile;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class apacheCamelControllerFileToFile {
    @Autowired
    private copyAllFilesRouteFileToFile copyAllFilesRouteFileToFile;
    @Autowired
    private copySpecificFilesRouteFileToFile copySpecificFilesRouteFileToFile;
    @Autowired
    private copySpecificFilesWithContentRouteFileToFile copySpecificFilesWithContentRouteFileToFile;

    @RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/fileToFile/hello")
    public String apacheCamelFileToFileHello(String[] args) {
        CamelContext helloWorldContext = new DefaultCamelContext();

        try {
            helloWorldContext.addRoutes(new helloWorldRouteFileToFile());
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerFileToFile's apacheCamelFileToFileHello method");
        }

        return "Printed Apache Camel's fileToFile message on console";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/fileToFile/copyAllFiles/source-{source}-destination-{destination}")
    public String apacheCamelFileToFileCopyAllFiles(@PathVariable String source, @PathVariable String destination) {
        CamelContext copyAllFilesContext = new DefaultCamelContext();

        try {
            source = source.replace("&", "/");
            destination = destination.replace("&", "/");
            copyAllFilesRouteFileToFile.setSource(source);
            copyAllFilesRouteFileToFile.setDestination(destination);
            copyAllFilesContext.addRoutes(copyAllFilesRouteFileToFile);
            copyAllFilesContext.start();
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerFileToFile's apacheCamelFileToFileCopyAllFiles method");
        }

        return "All files from source folder = \"" + copyAllFilesRouteFileToFile.getSource() + "\" are copied to destination folder = \"" + copyAllFilesRouteFileToFile.getDestination()+"\"";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/fileToFile/copySpecificFiles/source-{source}-destination-{destination}-fileName-{fileName}")
    public String apacheCamelFileToFileCopySpecificFiles(@PathVariable String source, @PathVariable String destination, @PathVariable String fileName) {
        CamelContext copySpecificFilesContext = new DefaultCamelContext();

        try {
            source = source.replace("&", "/");
            destination = destination.replace("&", "/");
            copySpecificFilesRouteFileToFile.setSource(source);
            copySpecificFilesRouteFileToFile.setDestination(destination);
            copySpecificFilesRouteFileToFile.setFilter(fileName);
            copySpecificFilesContext.addRoutes(copySpecificFilesRouteFileToFile);
            copySpecificFilesContext.start();
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerFileToFile's apacheCamelFileToFileCopySpecificFiles method");
        }

        return "All files with file name containing = \""+ copySpecificFilesRouteFileToFile.getFilter() +"\" from source folder = \"" + copySpecificFilesRouteFileToFile.getSource() + "\" are copied to destination folder = \"" + copySpecificFilesRouteFileToFile.getDestination()+"\"";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/fileToFile/copySpecificFilesWithContent/source-{source}-destination-{destination}-content-{content}")
    public String apacheCamelFileToFileCopySpecificFilesWithContent(@PathVariable String source, @PathVariable String destination, @PathVariable String content) {
        CamelContext copySpecificFilesWithContentContext = new DefaultCamelContext();

        try {
            source = source.replace("&", "/");
            destination = destination.replace("&", "/");
            copySpecificFilesWithContentRouteFileToFile.setSource(source);
            copySpecificFilesWithContentRouteFileToFile.setDestination(destination);
            copySpecificFilesWithContentRouteFileToFile.setFilter(content);
            copySpecificFilesWithContentContext.addRoutes(copySpecificFilesWithContentRouteFileToFile);
            copySpecificFilesWithContentContext.start();
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerFileToFile's apacheCamelFileToFileCopySpecificFilesWithContent method");
        }

        return "All files with content = \""+ copySpecificFilesWithContentRouteFileToFile.getFilter() +"\" from source folder = \"" + copySpecificFilesWithContentRouteFileToFile.getSource() + "\" are copied to destination folder = \"" + copySpecificFilesWithContentRouteFileToFile.getDestination()+"\"";
    }
}
