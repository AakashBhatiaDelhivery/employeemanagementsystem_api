package org.example.samplePackage.apacheCamel.fileToRabbitMQ;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@RestController
public class apacheCamelControllerFileToRabbitMQ {
    @Autowired
    private org.example.samplePackage.apacheCamel.fileToRabbitMQ.copyAllFilesRouteFileToRabbitMQ copyAllFilesRouteFileToRabbitMQ;
    @Autowired
    private org.example.samplePackage.apacheCamel.fileToRabbitMQ.copySpecificFilesRouteFileToRabbitMQ copySpecificFilesRouteFileToRabbitMQ;
    @Autowired
    private org.example.samplePackage.apacheCamel.fileToRabbitMQ.copySpecificFilesWithContentRouteFileToRabbitMQ copySpecificFilesWithContentRouteFileToRabbitMQ;

    @RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/fileToRabbitMQ/hello")
    public String apacheCamelFileToRabbitMQHello(String[] args) {
        CamelContext helloWorldContext = new DefaultCamelContext();

        try {
            helloWorldContext.addRoutes(new helloWorldRouteFileToRabbitMQ());
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerFileToRabbitMQ's apacheCamelFileToRabbitMQHello method");
        }

        return "Printed Apache Camel's RabbitMQ message on console";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/fileToRabbitMQ/copyAllFiles/source-{source}")
    public String apacheCamelCopyAllFilesFileToRabbitMQ(@PathVariable String source) throws IOException, TimeoutException {
        CamelContext copyAllFilesContext = new DefaultCamelContext();

        try {
            source = source.replace("&", "/");

            copyAllFilesRouteFileToRabbitMQ.setSource(source);
            copyAllFilesRouteFileToRabbitMQ.send();
            /*copyAllFilesContext.addRoutes(copyAllFilesRouteFileToRabbitMQ);
            copyAllFilesContext.start();*/
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerFileToRabbitMQ's apacheCamelCopyAllFiles method");
        }

        return "All files from source folder = \"" + copyAllFilesRouteFileToRabbitMQ.getSource() + "\" are copied ";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/fileToRabbitMQ/copySpecificFiles/source-{source}-fileName-{fileName}")
    public String apacheCamelCopySpecificFilesToRabbitMQ(@PathVariable String source, @PathVariable String fileName) {
        CamelContext copySpecificFilesContext = new DefaultCamelContext();

        try {
            source = source.replace("&", "/");
            copySpecificFilesRouteFileToRabbitMQ.setSource(source);
            copySpecificFilesRouteFileToRabbitMQ.setFilter(fileName);
            copySpecificFilesRouteFileToRabbitMQ.send();
            /*copySpecificFilesContext.addRoutes(copySpecificFilesRouteFileToRabbitMQ);
            copySpecificFilesContext.start();*/
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerFileToRabbitMQ's apacheCamelCopySpecificFilesToRabbitMQ method");
        }

        return "All files with file name containing = \""+ copySpecificFilesRouteFileToRabbitMQ.getFilter() +"\" from source folder = \"" + copySpecificFilesRouteFileToRabbitMQ.getSource() + "\" are copied";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/apacheCamel/fileToRabbitMQ/copySpecificFilesWithContent/source-{source}-content-{content}")
    public String apacheCamelCopySpecificFilesWithContentFileToRabbitMQ(@PathVariable String source, @PathVariable String content) {
        CamelContext copySpecificFilesWithContentContext = new DefaultCamelContext();

        try {
            source = source.replace("&", "/");
            copySpecificFilesWithContentRouteFileToRabbitMQ.setSource(source);
            copySpecificFilesWithContentRouteFileToRabbitMQ.setFilter(content);
            copySpecificFilesWithContentRouteFileToRabbitMQ.send();
            /*copySpecificFilesWithContentContext.addRoutes(copySpecificFilesWithContentRouteFileToRabbitMQ);
            copySpecificFilesWithContentContext.start();*/
        } catch (Exception e) {
            System.out.println("Exception occurred in apacheCamelControllerFileToRabbitMQ's apacheCamelCopySpecificFilesWithContentFileToRabbitMQ method");
        }

        return "All files with content = \""+ copySpecificFilesWithContentRouteFileToRabbitMQ.getFilter() +"\" from source folder = \"" + copySpecificFilesWithContentRouteFileToRabbitMQ.getSource() + "\" are copied ";
    }
}
