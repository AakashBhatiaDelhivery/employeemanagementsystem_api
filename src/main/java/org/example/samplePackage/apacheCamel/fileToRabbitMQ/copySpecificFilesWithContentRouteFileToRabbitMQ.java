package org.example.samplePackage.apacheCamel.fileToRabbitMQ;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class copySpecificFilesWithContentRouteFileToRabbitMQ /*extends RouteBuilder*/ {
    private String source, filter;

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingkey;

    public copySpecificFilesWithContentRouteFileToRabbitMQ() {
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    /*@Override
    public void configure() throws Exception {
        from("file:" + source + "?noop=true")
                .filter(body().convertToString().contains(filter))
                .to("file:" + destination);
    }*/

    public void send() throws IOException {
        File directory = new File(source);
        File[] list = directory.listFiles();
        for(File file : list) {
            if( Files.readString(Path.of(file.getPath())).contains(filter) ) {
                rabbitTemplate.convertAndSend(exchange, routingkey, Files.readString(Path.of(file.getPath()), StandardCharsets.US_ASCII));
                System.out.println("Send msg = " + file);
            }
        }
    }
}
