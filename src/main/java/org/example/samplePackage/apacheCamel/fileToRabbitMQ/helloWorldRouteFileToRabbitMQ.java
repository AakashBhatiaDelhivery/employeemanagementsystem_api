package org.example.samplePackage.apacheCamel.fileToRabbitMQ;

import org.apache.camel.builder.RouteBuilder;

public class helloWorldRouteFileToRabbitMQ extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        System.out.println("Hello World! this message is generated from helloWorldRoute of apacheCamel.fileToRabbitMQ package");
    }
}
