package org.example.samplePackage.repository;

import org.example.samplePackage.controller.Employee;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    @Transactional
    @Modifying
    @Query("update Employee set employeeDepartment = :employeeDepartment where employeeId = :employeeId")
    public void saveEmployeeDepartment(@Param("employeeId") int employeeId, @Param("employeeDepartment") String employeeDepartment);

    @Transactional
    @Modifying
    @Query("update Employee set employeeName = :employeeName     where employeeId = :employeeId")
    public void saveEmployeeName(@Param("employeeId") int employeeId, @Param("employeeName") String employeeName);
}
