package org.example.samplePackage.controller;

import org.example.samplePackage.employeeException.controller.employeeNotFound;
import org.example.samplePackage.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServices {
    @Autowired
    private EmployeeRepository employeeRepository;

    //private HashMap<Integer, Employee> employeesList;

    /*public EmployeeServices() {
        //employeesList = new HashMap<Integer, Employee>();
    }

    public HashMap<Integer, Employee> getEmployeesList() {
        return employeesList;
    }

    public boolean addEmployee(int employeeId, String employeeName, String employeeDepartment) {
        this.employeesList.put(employeeId, new Employee(employeeId, employeeName, employeeDepartment));
        return true;
    }

    public boolean removeEmployee(int employeeId) {
        this.employeesList.remove(employeeId);
        return true;
    }

    public boolean updateEmployeeName(int employeeId, String employeeName) {
        Employee emp = this.employeesList.get(employeeId);
        emp.setEmployeeName(employeeName);
        return true;
    }

    public boolean updateEmployeeDepartment(int employeeId, String employeeDepartment) {
        Employee emp = this.employeesList.get(employeeId);
        emp.setEmployeeDepartment(employeeDepartment);
        return true;
    }*/

    public List<Employee> getEmployeesList() {
        List<Employee> employeeList = new ArrayList<Employee>();
        employeeRepository.findAll().forEach(employeeList::add);

        if (employeeList.size() < 1)
            throw new employeeNotFound("No employees found!");

        return employeeList;
    }

    public boolean addEmployee(int employeeId, String employeeName, String employeeDepartment) {
        if (employeeRepository.findById(employeeId).isPresent())
            throw new employeeNotFound("Employee with id: " + employeeId + " already exists!");

        Employee employee = new Employee(employeeId, employeeName, employeeDepartment);
        employeeRepository.save(employee);
        return true;
    }

    public boolean removeEmployee(int employeeId) {
        if (!employeeRepository.findById(employeeId).isPresent())
            throw new employeeNotFound("Employee with id: " + employeeId + " do not exists!");

        employeeRepository.deleteById(employeeId);
        return true;
    }

    public boolean updateEmployeeName(int employeeId, String employeeName) {
        if (!employeeRepository.findById(employeeId).isPresent())
            throw new employeeNotFound("Employees with id: " + employeeId + " do not exists!");

        employeeRepository.saveEmployeeName(employeeId, employeeName);
        return true;
    }

    public boolean updateEmployeeDepartment(int employeeId, String employeeDepartment) {
        if (!employeeRepository.findById(employeeId).isPresent())
            throw new employeeNotFound("Employees with id: " + employeeId + " do not exists!");

        employeeRepository.saveEmployeeDepartment(employeeId, employeeDepartment);
        return true;
    }
}
