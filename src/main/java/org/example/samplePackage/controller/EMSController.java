package org.example.samplePackage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EMSController {
    @Autowired
    private EmployeeServices employeeServices;

    //by default it takes get request
    @RequestMapping("/hello")
    public String hello(String[] args) {
        return "Hello world!";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/hi")
    public String hi(String[] args) {
        return "Hi world!";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/sample/{parameter}")
    public String sample(@PathVariable("parameter") String text) {
        return "This is response to sample POST request with parameters as = " + text;
    }

    /*@RequestMapping("/getAllEmployees")
    public HashMap<Integer, Employee> getAllEmployees() {
        return employeeServices.getEmployeesList();
    }*/

    @RequestMapping("/addEmployee/{id}-{name}-{dep}")
    public boolean addEmployee(@PathVariable int id, @PathVariable String name, @PathVariable String dep) {
        return employeeServices.addEmployee(id, name, dep);
    }

    @RequestMapping("/removeEmployee/{id}")
    public boolean removeEmployee(@PathVariable int id) {
        return employeeServices.removeEmployee(id);
    }

    @RequestMapping("/updateEmployeeName/{id}-{name}")
    public boolean updateEmployeeName(@PathVariable int id, @PathVariable String name) {
        return employeeServices.updateEmployeeName(id, name);
    }

    @RequestMapping("/updateEmployeeDepartment/{id}-{dep}")
    public boolean updateEmployeeDepartment(@PathVariable int id, @PathVariable String dep) {
        return employeeServices.updateEmployeeDepartment(id, dep);
    }

    @RequestMapping("/getAllEmployees")
    public List<Employee> getAllEmployees() {
        return employeeServices.getEmployeesList();
    }
}
